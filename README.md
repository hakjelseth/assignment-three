# Assignment Three

This is the solution to assignment three. 
The solution includes an Movie characters API.

## Requirements
* .NET Core 5.0
* Microsoft  SQL Server Management Studio 

The connection string can be changed in appsettings.json.
  

## Participants
This assignment was completed by **Dawit Chala** and **Håkon Kjelseth**.
