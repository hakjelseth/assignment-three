﻿using Assignment_3.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.Models
{
    public class AssignmentThreeDbContext : DbContext
    {
        public AssignmentThreeDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source =ND-5CG9030MB1\\SQLEXPRESS; Initial Catalog = AssignmentThreeDb; Integrated Security = True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 1, Name = "Lord of the Rings", Description = "Hobbits try to destroy a ring" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 2, Name = "Kung fu panda", Description = "Fat panda, kung fuing" });
           
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 1, Title = "The fellowship of the ring", Genre = "Fantasy", ReleaseYear = 2001, Director = "Peter Jackson", Picture = "https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_.jpg", Trailer = "https://www.youtube.com/watch?v=V75dMMIW2B4" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 2, Title = "Kung fu Panda 1", Genre = "Fantasy" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 3, Title = "Kung fu Panda 2", Genre = "Fantasy" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 4, Title = "Kung fu Panda 3", Genre = "Fantasy" });

            modelBuilder.Entity<Character>().HasData(new Character { Id = 1, FullName = "Po" });
            modelBuilder.Entity<Character>().HasData(new Character { Id  = 2, FullName = "Master Shifu" });
            
            modelBuilder.Entity<Movie>()
                .HasMany(p => p.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacters",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("CharacterId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 2 },
                            new { CharacterId = 2, MovieId = 2 }
                        );
                    });

        }
    }
}
