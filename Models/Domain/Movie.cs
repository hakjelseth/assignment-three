﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.Models.Domain
{
    [Table("Movie")]
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        [MaxLength(50)]
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        [MaxLength(255)]
        public string Picture { get; set; }
        [MaxLength(50)]
        public string Trailer { get; set; }
        public ICollection<Character> Characters { get; set; }
        public Franchise Franchise { get; set; }
    }
}
